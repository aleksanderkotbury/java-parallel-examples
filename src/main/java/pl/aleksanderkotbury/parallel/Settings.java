package pl.aleksanderkotbury.parallel;

public class Settings {
    public static final int PARALLELISM = 20;

    private Settings() {
    }
}
