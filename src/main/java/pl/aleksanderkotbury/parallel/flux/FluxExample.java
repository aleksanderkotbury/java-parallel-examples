package pl.aleksanderkotbury.parallel.flux;

import pl.aleksanderkotbury.parallel.Processor;
import pl.aleksanderkotbury.parallel.Runner;
import pl.aleksanderkotbury.parallel.Settings;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

public class FluxExample {

    public static void main(String[] args) {
        Runner.INSTANCE.run(FluxExample::run);
    }

    private static void run() {
        var processor = new Processor();
        var testScheduler = Schedulers.newParallel("test", Settings.PARALLELISM);
        var sum = Flux.range(0, 200)
                .parallel(Settings.PARALLELISM)
                .runOn(testScheduler)
                .map(processor::process)
                .sequential()
                .toStream()
                .mapToInt(i -> i)
                .sum();

        System.out.println(sum);

        testScheduler.dispose();
    }
}
