package pl.aleksanderkotbury.parallel.stream;

import pl.aleksanderkotbury.parallel.Processor;
import pl.aleksanderkotbury.parallel.Runner;
import pl.aleksanderkotbury.parallel.Settings;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.IntStream;

public class StreamExample {

    public static void main(String[] args) {
        Runner.INSTANCE.run(StreamExample::wrappedRun);
    }

    private static void wrappedRun() {
        try {
            run();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    private static void run() throws InterruptedException, ExecutionException {
        new ForkJoinPool(Settings.PARALLELISM)
                .submit(StreamExample::execute)
                .get();
    }

    private static void execute() {
        Processor processor = new Processor();
        long sum = IntStream.range(0, 200)
                .parallel()
                .map(processor::process)
                .sum();

        System.out.println("Sum: " + sum);
    }
}
