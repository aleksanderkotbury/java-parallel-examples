package pl.aleksanderkotbury.parallel.rx;

import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;
import pl.aleksanderkotbury.parallel.Processor;
import pl.aleksanderkotbury.parallel.Runner;
import pl.aleksanderkotbury.parallel.Settings;

import java.util.List;
import java.util.concurrent.ForkJoinPool;

public class JavaRxExample {

    public static void main(String[] args) {
        Runner.INSTANCE.run(JavaRxExample::run);
    }

    private static void run() {
        var processor = new Processor();

        List<Integer> results = Flowable.range(0, 200)
                .parallel(Settings.PARALLELISM)
                .runOn(Schedulers.from(new ForkJoinPool(Settings.PARALLELISM)))
                .map(processor::process)
                .sequential()
                .toList()
                .blockingGet();

        var sum = results.stream().mapToInt(i -> i).sum();
        System.out.println(sum);
    }
}
