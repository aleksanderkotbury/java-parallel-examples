package pl.aleksanderkotbury.parallel.rx;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.schedulers.Schedulers;
import pl.aleksanderkotbury.parallel.Processor;
import pl.aleksanderkotbury.parallel.Runner;
import pl.aleksanderkotbury.parallel.Settings;

import java.util.List;
import java.util.concurrent.ForkJoinPool;

public class JavaRxFlatExample {
    private static final Processor processor = new Processor();

    public static void main(String[] args) {
        Runner.INSTANCE.run(JavaRxFlatExample::run);
    }

    private static void run() {
        List<Integer> results = Flowable.range(0, 200)
                .parallel(Settings.PARALLELISM)
                .runOn(Schedulers.from(new ForkJoinPool(Settings.PARALLELISM)))
                .flatMap(JavaRxFlatExample::processorWrapper)
                .sequential()
                .toList()
                .blockingGet();

        var sum = results.stream().mapToInt(i -> i).sum();
        System.out.println(sum);
    }

    private static Flowable<Integer> processorWrapper(int number) {
        return Single.create((SingleEmitter<Integer> subscriber) -> {
            int process = processor.process(number);
            subscriber.onSuccess(process);
        }).toFlowable();
    }
}
