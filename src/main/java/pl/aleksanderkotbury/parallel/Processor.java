package pl.aleksanderkotbury.parallel;

import java.time.LocalDateTime;

public class Processor {

    public int process(int number) {
        System.out.println(LocalDateTime.now() + " Calling process " + number);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return number;
    }
}
