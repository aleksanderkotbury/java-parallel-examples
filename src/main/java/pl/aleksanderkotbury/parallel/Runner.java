package pl.aleksanderkotbury.parallel;

import java.time.Duration;
import java.time.LocalDateTime;

public class Runner {

    public static final Runner INSTANCE = new Runner();

    public void run(Runnable runnable) {
        var start = LocalDateTime.now();

        runnable.run();

        var end = LocalDateTime.now();
        var duration = Duration.between(start, end);
        System.out.println("Duration in ms " + duration.toMillis());
    }
}
